﻿using Microsoft.Data.Sqlite;
using System.Data.Common;

namespace StudioKit.Tests;

public static class DbConnectionFactory
{
	public static DbConnection CreateSqliteInMemoryConnection()
	{
		var connection = new SqliteConnection("Filename=:memory:");
		return connection;
	}
}