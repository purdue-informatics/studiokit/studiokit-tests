﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Data.Common;

namespace StudioKit.Tests;

public static class DbContextFactory<TContext>
	where TContext : DbContext, new()
{
	private static TContext CreateDbContext(DbContextOptions options)
	{
		var dbContext = (TContext)Activator.CreateInstance(typeof(TContext), options);
		return dbContext;
	}

	public static TContext CreateTransient(DbConnection existingConnection = null)
	{
		var dbConnection = existingConnection ?? DbConnectionFactory.CreateSqliteInMemoryConnection();
		if (existingConnection == null)
			dbConnection.Open();
		var dbContextOptions = new DbContextOptionsBuilder<TContext>()
			.UseSqlite(dbConnection, o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery))
			.Options;
		var dbContext = CreateDbContext(dbContextOptions);
		dbContext.Database.EnsureCreated();
		return dbContext;
	}

	public static (TContext, IDbContextTransaction) CreateTransientAndTransaction(DbConnection existingConnection = null)
	{
		var dbContext = CreateTransient(existingConnection);
		var transaction = dbContext.Database.BeginTransaction(IsolationLevel.Serializable);
		return (dbContext, transaction);
	}
}