using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Moq;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Sharding;
using StudioKit.Sharding.Interfaces;
using StudioKit.Utilities;
using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Tests;

public class BaseTestDependencies<TContext> : IDisposable
	where TContext : DbContext, new()
{
	#region Readonly Properties

	public readonly string DatabaseId = Guid.NewGuid().ToString();

	public static readonly DateTime StartDate = new(2017, 12, 31);

	#endregion Readonly Properties

	#region Instance Properties

	/// <summary>
	/// Persistent connection that allows the same in-memory db to be used across test methods (transactions)
	/// </summary>
	private DbConnection _dbConnection;

	/// <summary>
	/// Persisted func used to construct the DbContext using <see cref="_dbConnection"/> in <see cref="PersistentDbContextFactory"/>, to be used again in <see cref="Dispose"/>
	/// </summary>
	private Func<object[], object[]> _addAdditionalArgs;

	public DateTime DateTime { get; private set; }
	public DynamicDateTimeProvider DateTimeProvider { get; }

	public IShardKeyProvider ShardKeyProvider { get; }
	public ILogger Logger { get; }
	public Mock<IErrorHandler> ErrorHandlerMock { get; }
	public IErrorHandler ErrorHandler { get; }

	#endregion Instance Properties

	public BaseTestDependencies(DateTime dateTime)
	{
		DateTimeProvider = new DynamicDateTimeProvider
		{
			UtcNow = dateTime
		};

		Reset(dateTime);

		var mockShardKeyProvider = new Mock<IShardKeyProvider>();
		mockShardKeyProvider.Setup(x => x.GetShardKey(It.IsAny<bool>())).Returns("test-shard");
		ShardKeyProvider = mockShardKeyProvider.Object;
		Logger = new Mock<ILogger>().Object;

		ErrorHandlerMock = new Mock<IErrorHandler>();
		ErrorHandler = ErrorHandlerMock.Object;
	}

	/// <summary>
	/// Reset anything that could have been changed between tests (mostly data)
	/// </summary>
	/// <param name="dateTime"></param>
	public virtual void Reset(DateTime dateTime)
	{
		SetDate(dateTime);
		ErrorHandlerMock?.Invocations.Clear();
	}

	/// <summary>
	/// To update the "now" DateTime in these test dependencies, use this method
	/// </summary>
	/// <param name="dateTime">The new "now"</param>
	public void SetDate(DateTime dateTime)
	{
		DateTime = dateTime;
		DateTimeProvider.UtcNow = dateTime;
	}

	#region DbConnection and DbContext

	private static TContext CreateDbContext(object[] args)
	{
		var dbContext = (TContext)Activator.CreateInstance(typeof(TContext),
			BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance | BindingFlags.OptionalParamBinding,
			null,
			args,
			CultureInfo.CurrentCulture);
		return dbContext;
	}

	private static DbContextOptions<TContext> CreateDbContextOptions(DbConnection dbConnection)
	{
		var dbContextOptionsBuilder = new DbContextOptionsBuilder<TContext>()
			.EnableSensitiveDataLogging();

		switch (dbConnection)
		{
			case SqliteConnection:
				dbContextOptionsBuilder = dbContextOptionsBuilder.UseSqlite(dbConnection,
					o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery));
				break;

			case SqlConnection:
				dbContextOptionsBuilder = dbContextOptionsBuilder.UseSqlServer(dbConnection, o =>
				{
					o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery);
				});
				break;
		}

		var dbContextOptions = dbContextOptionsBuilder.Options;
		return dbContextOptions;
	}

	private object[] CreateDbContextArgs(DbConnection dbConnection, Func<object[], object[]> addAdditionalArgs = null)
	{
		var args = new object[] { CreateDbContextOptions(dbConnection), DateTimeProvider };
		if (addAdditionalArgs != null)
		{
			args = addAdditionalArgs(args);
		}

		return args;
	}

	public TContext TransientDbContextFactory(Func<object[], object[]> addAdditionalArgs = null)
	{
		var dbConnection = DbConnectionFactory.CreateSqliteInMemoryConnection();
		dbConnection.Open();
		var dbContext = CreateDbContext(CreateDbContextArgs(dbConnection, addAdditionalArgs));
		dbContext.Database.EnsureCreated();
		return dbContext;
	}

	public TContext PersistentDbContextFactory(bool shouldUseSqlServer = false, Func<object[], object[]> addAdditionalArgs = null)
	{
		var isFirstOpen = _dbConnection == null;
		if (isFirstOpen)
		{
			if (shouldUseSqlServer)
			{
				var sqlServerName = EncryptedConfigurationManager.GetSetting(ShardAppSetting.SqlServerName);
				var databaseName = $"UnitTests-{DatabaseId}";
				var connectionString = $"{ShardConfiguration.GetConnectionString(sqlServerName, databaseName)};Persist Security Info=true";
				_dbConnection = new SqlConnection(connectionString);
			}
			else
			{
				_dbConnection = DbConnectionFactory.CreateSqliteInMemoryConnection();
				_dbConnection.Open();
			}
		}

		_addAdditionalArgs = addAdditionalArgs;
		var dbContext = CreateDbContext(CreateDbContextArgs(_dbConnection, addAdditionalArgs));
		if (isFirstOpen && shouldUseSqlServer)
		{
			dbContext.Database.EnsureDeleted();
		}

		dbContext.Database.EnsureCreated();
		return dbContext;
	}

	public (TContext, IDbContextTransaction) PersistentDbContextAndTransactionFactory(bool shouldUseSqlServer = false,
		Func<object[], object[]> addAdditionalArgs = null)
	{
		var dbContext = PersistentDbContextFactory(shouldUseSqlServer, addAdditionalArgs);
		var transaction = dbContext.Database.BeginTransaction(IsolationLevel.Serializable);

		// reload seeded entities from the db
		LoadSeededEntities(dbContext);

		return (dbContext, transaction);
	}

	public virtual void Dispose()
	{
		switch (_dbConnection)
		{
			case null:
				return;

			case SqlConnection:
			{
				var dbContext = PersistentDbContextFactory(shouldUseSqlServer: true, _addAdditionalArgs);
				dbContext.Database.EnsureDeleted();
				break;
			}
		}

		_dbConnection.Close();
		_dbConnection.Dispose();
	}

	protected virtual void LoadSeededEntities(TContext dbContext)
	{
	}

	#endregion DbConnection and DbContext

	#region Seeding

	public virtual Task SeedDatabaseAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	#endregion Seeding
}