namespace StudioKit.Tests;

/// <summary>
/// Expose constant FluentValidation Validator Names used in <see cref="FluentValidation.Resources.EnglishLanguage"/>
/// </summary>
public static class ValidatorName
{
	public const string EmailValidator = "EmailValidator";
	public const string GreaterThanOrEqualValidator = "GreaterThanOrEqualValidator";
	public const string GreaterThanValidator = "GreaterThanValidator";
	public const string LengthValidator = "LengthValidator";
	public const string MinimumLengthValidator = "MinimumLengthValidator";
	public const string MaximumLengthValidator = "MaximumLengthValidator";
	public const string LessThanOrEqualValidator = "LessThanOrEqualValidator";
	public const string LessThanValidator = "LessThanValidator";
	public const string NotEmptyValidator = "NotEmptyValidator";
	public const string NotEqualValidator = "NotEqualValidator";
	public const string NotNullValidator = "NotNullValidator";
	public const string PredicateValidator = "PredicateValidator";
	public const string AsyncPredicateValidator = "AsyncPredicateValidator";
	public const string RegularExpressionValidator = "RegularExpressionValidator";
	public const string EqualValidator = "EqualValidator";
	public const string ExactLengthValidator = "ExactLengthValidator";
	public const string InclusiveBetweenValidator = "InclusiveBetweenValidator";
	public const string ExclusiveBetweenValidator = "ExclusiveBetweenValidator";
	public const string CreditCardValidator = "CreditCardValidator";
	public const string ScalePrecisionValidator = "ScalePrecisionValidator";
	public const string EmptyValidator = "EmptyValidator";
	public const string NullValidator = "NullValidator";
	public const string EnumValidator = "EnumValidator";

	// Additional fallback messages used by clientside validation integration.
	// ReSharper disable InconsistentNaming
	public const string Length_Simple = "Length_Simple";
	public const string MinimumLength_Simple = "MinimumLength_Simple";
	public const string MaximumLength_Simple = "MaximumLength_Simple";
	public const string ExactLength_Simple = "ExactLength_Simple";

	public const string InclusiveBetween_Simple = "InclusiveBetween_Simple";
	// ReSharper enable InconsistentNaming
}