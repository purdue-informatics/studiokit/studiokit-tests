﻿using FluentValidation;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace StudioKit.Tests;

public static class ExceptionUtils
{
	public static string ArgumentNullExceptionMessage(string parameterName) =>
		$"Value cannot be null. (Parameter '{parameterName}')";

	public static string ArgumentExceptionMessage(string message, string parameterName = null) =>
		$"{message}{(!string.IsNullOrWhiteSpace(parameterName) ? $" (Parameter '{parameterName}')" : "")}";

	/// <summary>
	/// creates a validation message of the form:
	/// "Validation failed: {Environment.NewLine} -- {property}: {validationMessage}"
	/// to match the FluentValidation messages. Multiple messages can be added together.
	/// </summary>
	/// <param name="messages">A fully formatted errormessage. See GenerateFluentValidationFormattedString</param>
	/// <returns></returns>
	public static string GenerateFluentValidationString(params string[] messages)
	{
		return messages.Aggregate($"Validation failed: ",
			(current, message) => current + $"{Environment.NewLine} -- {message} Severity: Error");
	}

	/// <summary>
	/// Creates the formatting that FluentValidation uses for its messages.
	/// For substitution values see <see cref="FluentValidation.Resources.EnglishLanguage"/>
	/// NOTE: use PredicateValidator for the "The specified condition was not met for" message
	/// </summary>
	/// <param name="validatorName">the name of validator, <see cref="ValidatorName"/></param>
	/// <param name="propertyName">the property referenced in the error, e.g. nameof(Problem.Id)</param>
	/// <param name="readablePropNameOverride">
	/// optional value to replace "{PropertyName}" in the message.
	/// If not provided, <paramref name="propertyName"/> is split at each capital letter (and optional period).
	/// </param>
	/// <param name="comparisonValue">optional value to replace "{ComparisonValue}" in some validation messages</param>
	/// <param name="minLength">optional value to replace "{MinLength}" in some validation messages</param>
	/// <param name="maxLength">optional value to replace "{MaxLength}" in some validation messages</param>
	/// <param name="totalLength">optional value to replace "{TotalLength}" in some validation messages</param>
	/// <returns></returns>
	public static string GenerateFluentValidationFormattedString(string validatorName, string propertyName = "",
		string readablePropNameOverride = null, string comparisonValue = "", int minLength = 0, int maxLength = 0, int totalLength = 0)
	{
		var readablePropName = Regex.Replace(propertyName, @"([a-z])\.?([A-Z])", "$1 $2");
		var formattedString = ValidatorOptions.Global.LanguageManager.GetString(validatorName)
			.Replace("{PropertyName}", readablePropNameOverride ?? readablePropName)
			.Replace("{ComparisonValue}", comparisonValue)
			.Replace("{MinLength}", minLength.ToString())
			.Replace("{MaxLength}", maxLength.ToString())
			.Replace("{TotalLength}", totalLength.ToString());

		return $"{propertyName}: {formattedString}";
	}

	/// <summary>
	/// creates the formatting that FluentValidation uses for its messages, using a Properties.String message
	/// </summary>
	/// <param name="validationMessage"></param>
	/// <param name="propertyName"></param>
	/// <returns></returns>
	public static string GenerateCustomFluentValidationFormattedString(string validationMessage, string propertyName = "")
	{
		return $"{propertyName}: {validationMessage}";
	}
}